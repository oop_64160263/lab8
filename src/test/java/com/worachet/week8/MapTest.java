package com.worachet.week8;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MapTest {
    @Test
    public void shouldCreateMapSuccess1(){
    Map Map1 = new Map();
    assertEquals(5, Map1.getWidth());
    assertEquals(5, Map1.getHeight());
    }
    @Test
    public void shouldCreateMapSuccess2(){
        Map Map1 = new Map(7, 10);
        assertEquals(7, Map1.getWidth());
        assertEquals(10, Map1.getHeight());
        }
}