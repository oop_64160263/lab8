package com.worachet.week8;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TreeTest {
    @Test
    public void shouldCreateTreeSuccess(){
        Tree tree = new Tree("Tree0", 11, 12);
        assertEquals("Tree0", tree.getName());
        assertEquals(11, tree.getX());
        assertEquals(12, tree.getY());
    }
}
