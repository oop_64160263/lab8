package com.worachet.week8;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ShapeTest {
    @Test
    public void shouldCreateRectangleSuccess(){
        rectangle rect = new rectangle(4, 10);
        assertEquals(4, rect.getWidth());
        assertEquals(10, rect.getHeight());
    }
    @Test
    public void shouldCreateCircleSuccess(){
        Circle circle = new Circle(3);
        assertEquals(3, circle.getRadius());
    }
    @Test
    public void shouldCreateTriangleSuccess(){
        Triangle triangle = new Triangle(6, 8, 10);
        assertEquals(6, triangle.getA());
        assertEquals(8, triangle.getB());
        assertEquals(10, triangle.getC());
    }
    
}
