package com.worachet.week8;

public class Circle {
    private int radius;
    public Circle(int radius){
        this.radius=radius;
    }
    public void print(){
        System.out.println(3.14*(radius*radius));
    }
    public void Perimeter(){
        System.out.println(2*3.14*radius);
    }
    public int getRadius(){
        return radius;
    }
}
