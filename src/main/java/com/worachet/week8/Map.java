package com.worachet.week8;

public class Map {
    private int width;
    private int height;
    public Map(int width,int height){
        this.width=width;
        this.height=height;
    }
    public Map(){
        this(5,5);
    }
    public void print(){
        for(int i=1;i<=width;i++){
            for(int j=1;j<=height;j++){
                System.out.print("-");
            }
            System.out.println();
        }
    }
    public int getHeight() {
        return height;
    }
    public int getWidth() {
        return width;
    }
}
