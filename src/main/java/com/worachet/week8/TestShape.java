package com.worachet.week8;

public class TestShape {
    public static void main(String[] args) {
        rectangle rect1 = new rectangle(10, 5);
        rect1.print();
        
        rectangle rect2 = new rectangle(5, 3);
        rect2.print();

        Circle circle1 = new Circle(1);
        circle1.print();

        Circle circle2 = new Circle(2);
        circle2.print();

        Triangle triangle1 = new Triangle(5, 5, 6);
        triangle1.print();

        rect1.Perimeter();
        rect2.Perimeter();
        circle1.Perimeter();
        circle2.Perimeter();
        triangle1.Perimeter();
    }
}
